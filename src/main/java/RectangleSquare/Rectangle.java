
package RectangleSquare;

/**
 *
 * @author akret
 */
public class Rectangle {
    private int width;
    private int height;
    
    public Rectangle(){}
    public Rectangle(int width, int height){
        this.width = width;
        this.height = height;
    }
    
    public int getWidth(){
        return this.width;
    }
    
    public void setWidth(int width){
        this.width=width;
    }
    
    public int getHeight(){
        return this.height;
    }
    
    public void setHeight(int height){
        this.height = height;
    }
    
    public int getArea(){
        return this.height * this.width;
    }
    
    public final static void setDimensions(Rectangle r, int w, int h){
        r.setWidth(w);
        r.setHeight(h);
    }
    
}
